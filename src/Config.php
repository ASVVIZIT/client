<?php
   namespace Stanislavboyko\Client;

  use Stanislavboyko\Client\Exception;
  use Stanislavboyko\Client\Exception\ClientException;

  class Config
  {
      protected $params = [];

      public function __construct(array $parameters = [])
	  {
      	 $this->params = $parameters;
	  }

      public function set(array $parameters): void
	  {
	  	 $this->params = array_merge($this->params, $parameters);
	  }

	  public function get(): array
	  {
	  	return $this->params;
	  }

	  public function first(string $name)
	  {
	  	if(!isset($this->params[$name])) {
	  		if($name === 'track_redirects') {
	  			return false;
			}
			if($name === 'user_agent') {
				return 'testing/1.0';
			}
	  		throw new ClientException('Parameter ' . $name . ' not found');
		}

	  	return $this->params[$name];
	  }

	  public function guzzle(): array
	  {
		  $options = [
			  'base_uri'        => $this->first('base_url'),
			  'timeout'         => $this->first('timeout'),
			  'track_redirects' => $this->first('track_redirects'),
			  'debug'           => $this->first('debug'),
			  'headers'         => [
				  'User-Agent' => $this->first('user_agent'),
				  'Content-Type' => "application/json",
				  "Accept" => "application/json",
				  "Authorization" => "Token " . $this->first('api_key'),
				  "X-Secret"  => $this->first('secret_key')

			  ]
		  ];


		  return $options;
	  }
  }