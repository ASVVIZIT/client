<?php
namespace Stanislavboyko\Client\Laravel;

use Illuminate\Support\ServiceProvider;
use Stanislavboyko\Client\Dadata\Client;

class ClientServiceProvider extends ServiceProvider
{
	public function __construct($dispatcher = null)
	{
		parent::__construct($dispatcher);
	}

	/**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../config/dclient.php' => config_path('mzcoding-client.php'),
        ]);
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
          $this->mergeConfigFrom(
            __DIR__ . '/../../config/dclient.php', 'dclient'
          );
          $this->app->bind(Client::class);
    }
}