<?php
namespace Stanislavboyko\Client\Laravel;

use Illuminate\Support\Facades\Facade;
use Stanislavboyko\Client\Dadata\Client;

class ClientFacade extends Facade
{
    /**
     * @return string
	 * @see \Stanislavboyko\Client\Client
     */
    protected static function getFacadeAccessor(): string
    {
        return Client::class;
    }
}
