<?php
namespace Stanislavboyko\Client\Dadata;

use GuzzleHttp\Exception\ClientException;
use Stanislavboyko\Client\Config;
use Stanislavboyko\Client\Client as BaseClient;

class Client 
{
	/**
	 * Return Dadata API client object
	 *
	 * @param string $url
	 * @param string $method
	 * @param array $params
	 *
	 * @return BaseClient
	 */
    public function getClient(string $url, string $method, array $params = []): BaseClient
    {
        $parameters = config('dclient');
        if(!$parameters) {
        	 $parameters = include __DIR__ . '/../../config/dclient.php';
		}

        $config     = new Config($parameters);
        $response   = new BaseClient($config);
        $res = $response->request($url, $method, $params);
        dd($res->getBody());
        $code = $response->getStatusCode();
        if($code !== 200 || $code !== 201) {
            throw new ClientException("Error exception: " . $code);
        }

        if ($code !== 429) {
            throw new ClientException($code . ' ' . $response->getReasonPhrase());
        }

        return $response;
    }
	 
}